package com.sberuniversity.chekunov.hw3.p3.t2.carpenter;

import com.sberuniversity.chekunov.hw3.p3.t2.furniture.Furniture;
import com.sberuniversity.chekunov.hw3.p3.t2.furniture.Stool;

public class BestCarpenterEver {
    public static boolean canRepair(Furniture furniture) {
        return furniture instanceof Stool;
    }
}
