package com.sberuniversity.chekunov.hw3.p3.t2.furniture;

public abstract class Furniture {
    private final String name;

    public String getName() {
        return name;
    }

    public Furniture(String name) {
        this.name = name;
    }
}
