package com.sberuniversity.chekunov.hw3.p3.t2;

import com.sberuniversity.chekunov.hw3.p3.t2.carpenter.BestCarpenterEver;
import com.sberuniversity.chekunov.hw3.p3.t2.furniture.Furniture;
import com.sberuniversity.chekunov.hw3.p3.t2.furniture.Stool;
import com.sberuniversity.chekunov.hw3.p3.t2.furniture.Table;

public class Test {
    public static void main(String[] args) {
        print(new Stool());
        print(new Table());
    }

    static void print(Furniture furniture) {
        if (BestCarpenterEver.canRepair(furniture)) {
            System.out.println(furniture.getName() + " можно починить");
        } else {
            System.out.println(furniture.getName() + " не могу починить");
        }
    }
}
