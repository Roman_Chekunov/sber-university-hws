package com.sberuniversity.chekunov.hw3.p3.t4;

public class Dog {
    private final String name;
    private Integer[] points;
    public String getName() {
        return name;
    }

    public Integer[] getPoints() {
        return points;
    }

    public void setPoints(Integer[] points) {
        this.points = points;
    }

    public Dog(String name) {
        this.name = name;
    }
}
