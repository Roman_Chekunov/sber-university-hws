package com.sberuniversity.chekunov.hw3.p3.t4;

public record Participant(String name) {
    @Override
    public String name() {
        if (name == null) {
            System.err.println("Имя не задано");
        }
        return name;
    }

}
