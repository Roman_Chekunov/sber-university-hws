package com.sberuniversity.chekunov.hw3.p3.t4;

import java.util.Scanner;

public class Contest {
    private final Scanner in;
    private Dog[] dogs;
    private Participant[] participants;

    public Contest() {
        in = new Scanner(System.in);
        int N = in.nextInt();
        in.nextLine();
        initParticipants(N);
        initDogs(N);
    }

    private void initParticipants(int n) {
        participants = new Participant[n];
        System.out.println("Введите имена участников:");
        for (int i = 0; i < n; i++) {
            System.out.printf("%d: ", i);
            participants[i] = new Participant(in.nextLine());
        }
    }

    private void initDogs(int n) {
        dogs = new Dog[n];
        System.out.println("Введите клички собак участников:");
        for (int i = 0; i < n; i++) {
            System.out.printf("%d: ", i);
            dogs[i] = new Dog(in.nextLine());
        }
        System.out.println("Введите баллы для каждой собаки:");
        for (int i = 0; i < n; i++) {
            System.out.printf("%d %s: ", i, dogs[i].getName());
            dogs[i].setPoints(new Integer[]{in.nextInt(), in.nextInt(), in.nextInt()});
        }
    }

    public String contestResults() {
        StringBuilder results = new StringBuilder();
        int[] indexes = new int[3];
        double max, tempMax;
        for (int j = 0; j < 3; j++) {
            max = 0;
            for (int i = 0; i < dogs.length; i++) {
                if (i > 0 && contains(indexes, i)) {
                    continue;
                }
                tempMax = pointsMid(dogs[i].getPoints());
                if (tempMax > max) {
                    max = tempMax;
                    indexes[j] = i;
                }
            }
            results.append(
                    String.format(
                            "%s: %s, %.1f\n",
                            participants[indexes[j]].name(),
                            dogs[indexes[j]].getName(),
                            max
                    )
            );
        }
        return results.toString();
    }

    private double pointsMid(Integer[] points) {
        float mid = 0F;
        if (points != null && points.length > 0) {
            for (int point : points) {
                mid += point;
            }
            mid /= points.length;
        }
        return ((int)(mid * 10))/10.0;
    }
    private boolean contains(int[] array, int elem) {
        for (int i: array) {
            if (i == elem) {
                return true;
            }
        }
        return false;
    }
}
