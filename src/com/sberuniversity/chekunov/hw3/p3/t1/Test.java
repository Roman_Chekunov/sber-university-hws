package com.sberuniversity.chekunov.hw3.p3.t1;

import com.sberuniversity.chekunov.hw3.p3.t1.animals.Bat;
import com.sberuniversity.chekunov.hw3.p3.t1.animals.Dolphin;
import com.sberuniversity.chekunov.hw3.p3.t1.animals.Eagle;
import com.sberuniversity.chekunov.hw3.p3.t1.animals.GoldFish;
import com.sberuniversity.chekunov.hw3.p3.t1.detachment.Animal;

public class Test {
    public static void main(String[] args) {
        print(new Dolphin());
        print(new Bat());
        print(new Eagle());
        print(new GoldFish());
    }

    public static void print(Animal animal) {
        animal.eat();
        animal.sleep();
        animal.move();
        animal.wayOfBirth();
    }
}
