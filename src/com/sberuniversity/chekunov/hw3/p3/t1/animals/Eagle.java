package com.sberuniversity.chekunov.hw3.p3.t1.animals;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl.Bird;
import com.sberuniversity.chekunov.hw3.p3.t1.movement.Flying;

public class Eagle extends Bird implements Flying {
    public Eagle() {
        super("Орёл", "быстро", Flying.movementType);
    }
}
