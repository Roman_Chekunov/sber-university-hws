package com.sberuniversity.chekunov.hw3.p3.t1.animals;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl.Mammal;
import com.sberuniversity.chekunov.hw3.p3.t1.movement.Flying;

public class Bat extends Mammal implements Flying {
    public Bat() {
        super("Летучая мышь", "медленно", Flying.movementType);
    }
}
