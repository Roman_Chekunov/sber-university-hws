package com.sberuniversity.chekunov.hw3.p3.t1.animals;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl.Fish;

public class GoldFish extends Fish {

    public GoldFish() {
        super("Золотая рыбка", "медленно");
    }
}
