package com.sberuniversity.chekunov.hw3.p3.t1.animals;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl.Mammal;
import com.sberuniversity.chekunov.hw3.p3.t1.movement.Flying;
import com.sberuniversity.chekunov.hw3.p3.t1.movement.Swimming;

public class Dolphin extends Mammal implements Swimming {
    public Dolphin() {
        super("Дельфин", "быстро", Flying.movementType);
    }
}
