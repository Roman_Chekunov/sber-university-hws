package com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.Animal;

public abstract class Bird extends Animal {
    public Bird(String name, String movementSpeed, String movementType) {
        super(name, movementSpeed, movementType);
    }

    @Override
    public void wayOfBirth() {
        System.out.println(name + " откладывает яйца");
    }
}
