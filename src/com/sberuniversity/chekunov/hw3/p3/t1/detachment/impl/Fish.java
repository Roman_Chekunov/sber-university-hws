package com.sberuniversity.chekunov.hw3.p3.t1.detachment.impl;

import com.sberuniversity.chekunov.hw3.p3.t1.detachment.Animal;
import com.sberuniversity.chekunov.hw3.p3.t1.movement.Swimming;

public abstract class Fish extends Animal implements Swimming {
    public Fish(String name, String movementSpeed) {
        super(name, movementSpeed, Swimming.movementType);
    }

    @Override
    public void wayOfBirth() {
        System.out.println(name + " мечет икру");
    }
}
