package com.sberuniversity.chekunov.hw3.p3.t1.detachment;

public abstract class Animal {
    protected String name;
    protected String movementSpeed;
    protected String movementType;

    public Animal(String name, String movementSpeed, String movementType) {
        this.name = name;
        this.movementSpeed = movementSpeed;
        this.movementType = movementType;
    }

    public final void sleep() {
        System.out.println(name + " делает сладкий sleep");
    }

    public final void eat() {
        System.out.println(name + " кушает");
    }

    public abstract void wayOfBirth();

    public void move() {
        System.out.printf("%s %s %s\n", name, movementType, movementSpeed);
    }
}
