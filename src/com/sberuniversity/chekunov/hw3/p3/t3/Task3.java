package com.sberuniversity.chekunov.hw3.p3.t3;

import java.util.ArrayList;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        task(sc.nextInt(), sc.nextInt());
    }

    static void task(int N, int M) {
        if (N > 0 && N < 100) {
            if (M > 0 && M < 100) {
                printMatrix(indexSum(N, M));
            } else {
                System.out.println("Колличество строк задано неправильно, допустимый диапозон: (0;100)");
            }
        } else {
            System.out.println("Колличество столбцов задано неправильно, допустимый диапозон: (0;100)");
        }
    }

    public static ArrayList<ArrayList<Integer>> indexSum(int N, int M) {
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
        ArrayList<Integer> row;
        for (int i = 0; i < M; i++) {
            row = new ArrayList<>();
            for (int j = 0; j < N; j++) {
                row.add(i + j);
            }
            matrix.add(row);
        }
        return matrix;
    }

    static void printMatrix(ArrayList<ArrayList<Integer>> matrix) {
        for (ArrayList<Integer> row : matrix) {
            StringBuilder rowString = new StringBuilder();
            for (int elem: row) {
                rowString.append(elem).append(" ");
            }
            System.out.println(rowString);
        }
    }
}
