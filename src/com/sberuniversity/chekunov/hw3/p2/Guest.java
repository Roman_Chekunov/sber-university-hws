package com.sberuniversity.chekunov.hw3.p2;

public class Guest {
    private String name;

    private String bookName;

    private Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getBookName() {
        if (bookName == null) {
            System.out.println("Книга ещё не назначена");
        }
        return bookName;
    }

    private void setBookName(String bookName) {
        if (id == null) {
            id = Library.getGuestCount();
        }
        this.bookName = bookName;
    }

    public void giveBook(String bookName) {
        setBookName(bookName);
    }

    public void deleteBook() {
        setBookName(null);
    }

    public Guest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "name='" + name + '\'' +
                ", bookName='" + bookName + '\'' +
                ", id=" + id +
                '}';
    }
}
