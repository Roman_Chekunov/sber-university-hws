package com.sberuniversity.chekunov.hw3.p2;

import java.util.Objects;

public class Book {

    private final String name;
    private final String author;
    private boolean isTaken;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public void take() {
        isTaken = true;
    }

    public void retrieve() {
        isTaken = false;
    }

    public boolean isTaken() {
        return isTaken;
    }

    public Book(String name, String author) {
        this.name = name;
        this.author = author;
        this.isTaken = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(name, book.name) && Objects.equals(author, book.author);
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", isTaken=" + isTaken +
                '}';
    }
}
