package com.sberuniversity.chekunov.hw3.p2;

import java.util.Arrays;

public class Library {
    /**
     * Размерность массива при инициалзации
     */
    private final int CAPACITY = 20;

    /**
     * Во сколько раз увеличится размерность массива
     * при наполнении массива
     */
    private final float CAPACITY_MOD = 1.25F;

    /**
     * Колличество посетителей библиотеки. Выступает как выдаваемый ID
     */
    private static int guestCount;

    /**
     * Хранилище книг
     */
    private Book[] books;

    /**
     * Колличество книг в библиотеке
     */
    private int booksCount;

    private BookRating[] bookRatings;

    public static int getGuestCount() {
        return guestCount;
    }

    Library() {
        books = new Book[CAPACITY];
        bookRatings = new BookRating[CAPACITY];
        booksCount = 0;
        guestCount = 0;
    }

    /**
     * Добавляет книгу в библиотеку
     * @param book добавляемая книга
     */
    public void addBook(Book book) {
        for (Book b: books) {
            if (book.equals(b)) {
                System.out.println("Данная книга уже зарегистрированна в библиотеке");
                return;
            }
        }
        if (booksCount == CAPACITY) {
            books = Arrays.copyOf(books, (int) (booksCount * CAPACITY_MOD));
            bookRatings = Arrays.copyOf(bookRatings, (int) (booksCount * CAPACITY_MOD));
        }
        books[booksCount] = book;
        bookRatings[booksCount] = new BookRating(book);
        booksCount++;
    }

    /**
     * Удаление книги по названию
     * @param bookName название книги
     */
    public void deleteBookByName(String bookName) {
        boolean found = false;
        for (int i = 0; i < booksCount; i++) {
            if (found) {
                books[i - 1] = books[i];
                bookRatings[i - 1] = bookRatings[i];
                continue;
            }
            if (books[i].getName().equals(bookName)) {
                if (!books[i].isTaken()) {
                    found = true;
                } else {
                    System.out.println("Книга одолжена");
                }
            }
        }
        if (found) {
            books[--booksCount] = null;
            bookRatings[booksCount] = null;
        } else {
            System.out.printf("Книга \"%s\" не зарегестрированна в библиотеке\n", bookName);
        }
    }

    public void print() {
        for (Book book: books) {
            if (book != null) {
                System.out.println(book);
            }
        }
    }

    /**
     * Возвращает ссылку на книгу по названию в параметрах
     * @param bookName название книги
     * @return объект {@link Book}, либо null, если не найдена книга
     */
    public Book findBookByName(String bookName) {
        for (int i = 0; i < booksCount; i++) {
            if (bookName.equals(books[i].getName())) {
                return books[i];
            }
        }
        System.out.printf("Книга \"%s\" не найдена\n", bookName);
        return null;
    }

    /**
     * Возвращает массив книг, написанных автором, указанным в параметрах
     * @param author искомый автор
     * @return массив книг, написанных автором, указанным в параметрах
     */
    public Book[] findBooksByAuthor(String author) {
        Book[] books;
        int count = 0;
        for (int i = 0; i < booksCount; i++) {
            if (author.equals(this.books[i].getAuthor())) {
                count++;
            }
        }
        if (count > 0) {
            books = new Book[count];
            for (int i = 0, j = 0; i < booksCount; i++) {
                if (author.equals(this.books[i].getAuthor())) {
                    books[j] = this.books[i];
                    j++;
                }
            }
            return books;
        }
        System.out.println("Автор не найден");
        return null;
    }

    /**
     * Выдаёт книгу посетителю. Если посетитель берёт книгу впервые,
     * ему присваивается id.
     * @param book выдаваемая книга ({@link Book})
     * @param guest посетитель библиотеки ({@link Guest})
     * @return успешностсь операции. True - книга выдана, false - нет
     */
    public boolean give(Book book, Guest guest) {
        if (book != null) {
            for (int i = 0; i < booksCount; i++) {
                if (book.equals(books[i])
                        && guest.getBookName() == null
                        && !book.isTaken()) {
                    if (guest.getId() == null) {
                        guestCount++;
                    }
                    guest.giveBook(book.getName());
                    book.take();
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Устанавливает статус книги от гостя как незанятой,
     * удаляет книгу у посетителя
     * @param guest посетитель, у которого нужно забрать книгу
     * @return успешность операции. True - вернули книгу, False - нет
     */
    public boolean takeBackBy(Guest guest) {
        if (guest != null) {
            String guestBookName = guest.getBookName();
            if (guestBookName != null) {
                Book guestBook = findBookByName(guestBookName);
                if (guestBook != null) {
                    if (!guestBook.isTaken()) {
                        System.out.println("Книга уже в библиотеке");
                    } else {
                        guestBook.retrieve();
                        guest.deleteBook();
                        return true;
                    }
                } else {
                    System.out.println("Посетитель не одалживал книгу у библиотеки");
                }
            } else {
                System.out.println("Название книги не задано");
            }
        }
        return false;
    }

    /**
     * Устанавливает статус книги от гостя как незанятой,
     * удаляет книгу у посетителя
     * @param guest посетитель, у которого нужно забрать книгу
     * @param rate оценка книги от посетителя
     * @return успешность операции. True - вернули книгу, False - нет
     */
    public boolean takeBackWithRate(Guest guest, int rate) {
        if (guest != null) {
            String guestBookName = guest.getBookName();
            if (takeBackBy(guest)) {
                int index = findBookId(guestBookName);
                if (index > -1) {
                    bookRatings[index].rate(guest, rate);
                    return true;
                }
            }
        }
        return false;
    }

    private int findBookId(String bookName) {
        for (int i = 0; i < booksCount; i++) {
            if (bookName.equals(books[i].getName())) {
                return i;
            }
        }
        return -1;
    }

    public Double getBookRating(String bookName) {
        int index = findBookId(bookName);
        if (index > -1) {
            return ((int)(bookRatings[index].getRate() * 10)) / 10.0;
        }
        return null;
    }
}
