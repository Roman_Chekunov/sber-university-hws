package com.sberuniversity.chekunov.hw3.p2;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Library l = new Library();
        l.addBook(new Book("1", "a1"));
        l.addBook(new Book("2", "a2"));
        l.addBook(new Book("3", "a1"));
        l.print();
        l.deleteBookByName("2");
        l.print();

        System.out.println(l.findBookByName("1"));
        l.findBookByName("2");

        System.out.println(Arrays.toString(l.findBooksByAuthor("a1")));
        l.findBooksByAuthor("J.R.R. Tolkien");

        Guest guest = new Guest("reader");
        System.out.println(guest);
        giveBook(l, guest, "1");
        System.out.println(guest);

        Guest guest1 = new Guest("reader1");
        giveBook(l, guest, "1");
        giveBook(l, guest1, "3");
        System.out.println(guest1);

        l.print();
        takeBackBook(l, guest);
        takeBackBook(l, guest);
        l.print();

        giveBook(l, guest, "Lord of the Ring");
        takeBackBook(l, guest);

        giveBook(l, guest, "1");
        takeBackBook(l, guest, 4);
        l.getBookRating("1");
        takeBackBook(l, guest1, 7);
        giveBook(l, guest1, "1");
        takeBackBook(l, guest1, 1);
        System.out.println(l.getBookRating("1"));
    }

    static void takeBackBook(Library l, Guest guest) {
        if (l.takeBackBy(guest)) {
            System.out.printf("Посетитель [%s] успешно вернул книгу\n", guest.getName());
        } else {
            System.out.printf("Невозможно вернуть книгу от [%s]\n", guest.getName());
        }
    }

    static void giveBook(Library l, Guest guest, String book) {
        if (l.give(l.findBookByName(book), guest)) {
            System.out.printf("Книга [%s] успешно выдана посетителю [%s]\n", book, guest.getName());
        } else {
            System.out.printf("Книга [%s] не выдана посетителю [%s]\n", book, guest.getName());
        }
    }

    static void takeBackBook(Library l, Guest guest, int rate) {
        if (l.takeBackWithRate(guest, rate)) {
            System.out.printf("Посетитель [%s] успешно вернул книгу с оценкой [%d]\n", guest.getName(), rate);
        } else {
            System.out.printf("Невозможно вернуть книгу от [%s]\n", guest.getName());
        }
    }
}
