package com.sberuniversity.chekunov.hw3.p2;

import java.util.Arrays;

public class BookRating {

    private int CAPACITY = 4;
    private float capacityMod = 1.5F;
    private final Book book;
    private Guest[] guests;
    private int ratesCount;
    private int[] rates;
    private double rate;

    public Book getBook() {
        return book;
    }

    public double getRate() {
        return rate;
    }

    public BookRating(Book book) {
        this.book = book;
        guests = new Guest[CAPACITY];
        rates = new int[CAPACITY];
    }

    public boolean rate(Guest guest, int rate) {
        if (rate >= 0 && rate <= 5) {
            int i = 0;
                for (; i < guests.length; i++) {
                    if (guests[i] == null || guest.equals(guests[i])) {
                        break;
                    }
                }
            if (i == guests.length) {
                expand();
            }
            guests[i] = guest;
            rates[i] = rate;
            ratesCount++;
            calculateRate();
        }
        return false;
    }

    private void expand() {
        guests = Arrays.copyOf(guests, (int) (guests.length * capacityMod));
    }

    private void calculateRate() {
        rate = 0;
        for (int rate: rates) {
            this.rate += rate;
        }
        this.rate /= ratesCount;
    }
}