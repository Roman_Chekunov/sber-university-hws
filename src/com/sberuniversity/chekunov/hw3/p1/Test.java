package com.sberuniversity.chekunov.hw3.p1;

import com.sberuniversity.chekunov.hw3.p1.t7.TriangleChecker;

public class Test {
    public static void main(String[] args) {
        System.out.println(
                TriangleChecker.canBuildTriangle(1, 1, 1) + "\n" //true
                + TriangleChecker.canBuildTriangle(10, 5, 5) + "\n" //false
                + TriangleChecker.canBuildTriangle(10, 5, 6) + "\n" //true
                + TriangleChecker.canBuildTriangle(1, 10, 1) + "\n" //false
                + TriangleChecker.canBuildTriangle(0, 0, 1) + "\n" //false
        );
    }
}
