package com.sberuniversity.chekunov.hw3.p1.t4;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        addTime(hours, minutes, seconds);
        if (hours > 23) {
            System.out.println("Неправильно заданы часы. Будет установленно " + this.hours);
        }
        if (minutes > 59) {
            System.out.println("Неправильно заданы минуты. Будет установленно " + this.minutes);
        }
        if (seconds > 59) {
            System.out.println("Неправильно заданы секунды. Будет установленно " + this.seconds);
        }
    }

    public TimeUnit(int hours, int minutes) {
        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) {
        this(hours, 0, 0);
    }

    public void showTime24() {
        System.out.println(timeFormatted(false));
    }

    public void showTime12() {
        System.out.println(timeFormatted(true));
    }

    public void addTime(int hours, int minutes, int seconds) {
        if (seconds + this.seconds > 59) {
            minutes += (seconds + this.seconds) / 60;
            this.seconds = (seconds + this.seconds) % 60;
        } else {
            this.seconds += seconds;
        }
        if (minutes + this.minutes > 59) {
            hours += (minutes + this.minutes) / 60;
            this.minutes = (minutes + this.minutes) % 60;
        } else {
            this.minutes += minutes;
        }
        if (hours + this.hours > 23) {
            this.hours = (hours + this.hours) % 24;
        } else {
            this.hours += hours;
        }
    }

    private String timeFormatted(boolean is12hour) {
        String meridiem = "",
                hours,
                minutes,
                seconds;
        int hoursInt = this.hours;

        if (is12hour) {
            if (this.hours > 11) {
                hoursInt -= 12;
                meridiem = "pm";
            } else {
                meridiem = "am";
            }
        }
        if (hoursInt < 10) {
            hours = "0" + hoursInt;
        } else {
            hours = String.valueOf(hoursInt);
        }

        if (this.minutes < 10) {
            minutes = "0" + this.minutes;
        } else {
            minutes = String.valueOf(this.minutes);
        }

        if (this.seconds < 10) {
            seconds = "0" + this.seconds;
        } else {
            seconds = String.valueOf(this.seconds);
        }

        return String.format("%s:%s:%s %s", hours, minutes, seconds, meridiem);
    }
}
