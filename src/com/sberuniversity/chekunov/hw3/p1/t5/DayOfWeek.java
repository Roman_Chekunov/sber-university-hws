package com.sberuniversity.chekunov.hw3.p1.t5;

public class DayOfWeek {
    private final byte id;
    private final String day;

    public DayOfWeek(byte id, String day) {
        this.id = id;
        this.day = day;
    }

    public DayOfWeek(int id, String monday) {
        this((byte) id, monday);
    }

    @Override
    public String toString() {
        return id + " " + day;
    }
}
