package com.sberuniversity.chekunov.hw3.p1.t3;

import com.sberuniversity.chekunov.hw3.p1.t2.Student;
import java.util.Arrays;

public class StudentService {
    public static Student bestStudent(Student[] students) {
        Student bestStudent = students[0];
        for (int i = 1; i < students.length; i++) {
            if (students[i].midGrade() > bestStudent.midGrade()) {
                bestStudent = students[i];
            }
        }
        return bestStudent;
    }

    public static void sortBySurname(Student[] students) {
        Arrays.sort(students, (o1, o2) -> {
            int surnameLen = Math.min(o1.getSurname().length(), o2.getSurname().length());
            for (int i = 0; i < surnameLen; i++) {
                if (o1.getSurname().charAt(i) > o2.getSurname().charAt(i)) {
                    return 1;
                } else if (o1.getSurname().charAt(i) < o2.getSurname().charAt(i)) {
                    return -1;
                }
            }
            return 0;
        });
    }
}
