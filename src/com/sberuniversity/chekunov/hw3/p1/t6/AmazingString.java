package com.sberuniversity.chekunov.hw3.p1.t6;

public class AmazingString {
    private char[] literallyString;

    public AmazingString(char[] literallyString) {
        this.literallyString = literallyString;
    }

    public AmazingString(String string) {
        this.literallyString = stringToChars(string);
    }

    private char[] stringToChars(String string) {
        char[] chars = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            chars[i] = string.charAt(i);
        }
        return chars;
    }

    public char charAt(int index) {
        return literallyString[index];
    }

    public int length() {
        return literallyString.length;
    }

    public void print() {
        for (char ch: literallyString) {
            System.out.print(ch);
        }
        System.out.println();
    }

    public boolean haveSubstring(char[] chars) {
        for (int i = 0; i < length(); i++) {
            if (length() - chars.length - i < 0) {
                return false;
            }
            if (chars[0] == literallyString[i]) {
                for (int j = 1; j < chars.length; j++) {
                    if (chars[j] != literallyString[j + i]) {
                        break;
                    }
                    if (j == chars.length - 1) {
                        return true;
                    }
                }
            }
        }
        return true;
    }

    public boolean haveSubstring(String string) {
        return haveSubstring(stringToChars(string));
    }

    public void trimLeadingSpaces() {
        if (literallyString[0] == ' ') {
            int spaces = literallyString[length() - 1] == ' ' ? 2 : 1;
            char[] newString = new char[length() - spaces];
            for (int i = 0; i < length() - spaces; i++) {
                newString[i] = literallyString[i + 1];
            }
            literallyString = newString;
        } else if (literallyString[length() - 1] == ' ') {
            char[] newString = new char[length() - 1];
            for (int i = 0; i < length() - 1; i++) {
                newString[i] = literallyString[i];
            }
            literallyString = newString;
        }
    }

    public void reverse() {
        char temp;
        int tailIndex = length() - 1;
        for (int i = 0; i < length() / 2; i++) {
            temp = literallyString[i];
            literallyString[i] = literallyString[tailIndex - i];
            literallyString[tailIndex - i] = temp;
        }
    }
}
