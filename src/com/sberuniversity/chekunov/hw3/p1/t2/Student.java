package com.sberuniversity.chekunov.hw3.p1.t2;

import java.util.Arrays;

public class Student {

    private String name;
    private String surname;
    private int[] grades;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        if (grades.length > 10) {
            System.out.println("Допустим ввод только 10 последних оценок");
        } else {
            this.grades = grades;
        }
    }

    public void addGrade(int grade) {
        for (int i = 0; i < grades.length - 1; i++) {
            grades[i] = grades[i + 1];
        }
        grades[grades.length-1] = grade;
    }

    public int midGrade() {
        int sum = 0;
        for (int grade: grades) {
            sum += grade;
        }
        return sum / grades.length;
    }
}
