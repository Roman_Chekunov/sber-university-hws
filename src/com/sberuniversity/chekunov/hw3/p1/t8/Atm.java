package com.sberuniversity.chekunov.hw3.p1.t8;

public class Atm {
    private final double roublesPerDollar;
    private final double dollarsPerRouble;

    private static int counter;

    public Atm(double roublesPerDollar, double dollarsPerRouble) {
        this.roublesPerDollar = roublesPerDollar;
        this.dollarsPerRouble = dollarsPerRouble;
        counter++;
    }

    public double convertRubToUsd(double roubles) {
        return (int) (roubles * dollarsPerRouble * 100) / 100.0;
    }

    public double convertUsdToRub(double dollars) {
        return (int) (dollars * roublesPerDollar * 100) / 100.0;
    }

    public static int getCountOfInstances() {
        return counter;
    }
}
