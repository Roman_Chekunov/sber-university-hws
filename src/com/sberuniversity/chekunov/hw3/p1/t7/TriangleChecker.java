package com.sberuniversity.chekunov.hw3.p1.t7;

public class TriangleChecker {
    public static boolean canBuildTriangle(double a, double b, double c) {
        return a + b > c &&
                a + c > b &&
                b + c > a;
    }
}
