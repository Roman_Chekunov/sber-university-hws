package com.sberuniversity.chekunov.profile.hw3;

public class AnnotationChecker {
    public static <T> void check(T clazz) {
        IsLike annotation = clazz.getClass().getAnnotation(IsLike.class);
        try {
            System.out.printf("IsLike: %s\n", annotation.value());
        } catch (NullPointerException e) {
            System.err.println("Аннотация IsLike не присвоена этому классу");
        }
    }
}
