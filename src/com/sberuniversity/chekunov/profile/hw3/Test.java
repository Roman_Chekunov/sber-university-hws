package com.sberuniversity.chekunov.profile.hw3;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Test {
    @IsLike
    static class test1 {}

    @IsLike(false)
    static class test2 {}

    static class test3 {}

    public static void main(String[] args) {
        System.out.println(getAllInterfaces(ClB.class));
    }

    static void test1() {
        AnnotationChecker.check(new test1());
        AnnotationChecker.check(new test2());
        AnnotationChecker.check(new test3());
    }

    static void testAPrinter() {
        APrinter printer = new APrinter();
        tryToInvoke(printer, "print", Integer.MAX_VALUE);
        tryToInvoke(printer, "print", 0, 1);
        tryToInvoke(printer, "print", "0");
        tryToInvoke(printer, "print", null, null);
        tryToInvoke(printer, "print", null);
        tryToInvoke(printer, "(⚆_⚆)");
    }

    static <T> void tryToInvoke(T clazz, String methodName, Object... args) {
        Class<?> cls = clazz.getClass();
        Class<?>[] parameterTypes;
        try {
                parameterTypes = new Class<?>[args.length];
                for (int i = 0; i < args.length; i++) {
                    parameterTypes[i] = args[i].getClass();
                }
            cls.getDeclaredMethod(methodName, parameterTypes).invoke(clazz, args);
        } catch (
                NoSuchMethodException |
                InvocationTargetException |
                IllegalArgumentException |
                IllegalAccessException |
                NullPointerException |
                ExceptionInInitializerError e) {
            System.err.println(e);
        }
    }

    static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> list = new ArrayList<>();
        while (cls != Object.class && cls != null) {
            var interfaces = cls.getInterfaces();
            for (var inter : interfaces) {
                list.addAll(getAllInterfaces(inter));
            }
            list.addAll(Arrays.asList(interfaces));
            cls = cls.getSuperclass();
        }
        return list;
    }

    static boolean checkSequenceSimple(String str) {
        int counter = 0;
        char[] chars = str.toCharArray();
        for (char ch: chars) {
            if (ch == '(')
                counter++;
            else
                counter--;
            if (counter < 0) break;
        }
        return counter == 0;
    }

    static boolean checkSequence(String str) {
        Stack<Character> stack = new Stack<>();
        char[] chars = str.toCharArray();
        for (char ch : chars) {
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.push(ch);
            } else if (!stack.empty() && isMirrored(stack.peek(), ch)) {
                stack.pop();
            } else {
                return false;
            }
        }
        return stack.empty();
    }

    static boolean isMirrored(char ch1, char ch2) {
        return switch (ch1) {
            case '(' -> ch2 == ')';
            case '{' -> ch2 == '}';
            case '[' -> ch2 == ']';
            default -> false;
        };
    }
}

interface IntA {}
interface IntB {}
interface IntC extends IntB {}

class ClA implements IntA {}

class ClB extends ClA implements IntC{}
