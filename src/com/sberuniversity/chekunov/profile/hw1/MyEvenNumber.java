package com.sberuniversity.chekunov.profile.hw1;

public class MyEvenNumber {
    private final int n;

    public int getN() {
        return n;
    }

    public MyEvenNumber(int n) throws IllegalArgumentException {
        if (n % 2 != 0) {
            throw new IllegalArgumentException("Число нечётное!");
        }
        this.n = n;
    }
}
