package com.sberuniversity.chekunov.profile.hw1;

import java.util.Scanner;

public class Additional1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов ");
        int[] array = new int[sc.nextInt()];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("[%d] = ", i + 1);
            array[i] = sc.nextInt();
        }
        twoMax(array);
    }
    public static void twoMax(int[] array) {
        int max1 = array[0], max2 = Integer.MIN_VALUE;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max1) {
                max2 = max1;
                max1 = array[i];
            } else if (array[i] > max2) {
                max2 = array[i];
            }
        }
        System.out.printf("Первый максимум: %d\nВторой максимум: %d", max1, max2);
    }
}
