package com.sberuniversity.chekunov.profile.hw1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Copying {
    public static void main(String[] args) {
        try {
            copyWithUpperCase("input.txt", "output.txt");
        } catch (IOException e) {
            System.err.println("что-то пошло не так \n" + e.getMessage());
        }
    }

    public static void copyWithUpperCase(String src, String dest) throws IOException {
        try(FileInputStream input = new FileInputStream(src);
            FileOutputStream output = new FileOutputStream(dest))
        {
            int c;
            while((c = input.read()) != -1){
                if (c >= 97 && c <= 122) {
                    c -= 32;
                }
                output.write(c);
            }
        }
    }
}
