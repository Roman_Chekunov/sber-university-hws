package com.sberuniversity.chekunov.profile.hw1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    public static void checkName(String str) {
        if (str.length() > 20 || str.length() < 2) {
            throw new IllegalArgumentException("Неправильно задана длина имени! (от 2 до 20)");
        } else if (Character.isLowerCase(str.charAt(0))) {
            throw new IllegalArgumentException("Первая буква должна быть заглавной!");
        }
        System.out.println("checkName OK");
    }

    public static void checkBirthdate(String str) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Date input, min;
        try {
            input = format.parse(str);
            min = format.parse("01.01.1900");
        } catch (ParseException e) {
            System.err.println("Неправильный форматы даты!");
            throw new RuntimeException(e);
        }

        Date today = new Date();

        if (input.after(today) || input.before(min)) {
            throw new IllegalArgumentException("Неправильно задана дата!");
        }
        System.out.println("checkBirthdate OK");
    }

    public static void checkGender(String str) {
        if (!Gender.isInstance(str)) {
            throw new IllegalArgumentException("Такого гендера не существует >:D");
        }
        System.out.println("checkGender OK");
    }

    public static void checkHeight(String str) {
        double height;
        try {
            height = Double.parseDouble(str);
        } catch (RuntimeException e) {
            System.err.println("Не удалось преобразовать число");
            throw e;
        }
        if (height < 0) {
            throw new IllegalArgumentException("Рост не может быть отрицательным");
        }
        System.out.println("checkHeight OK");
    }
}
