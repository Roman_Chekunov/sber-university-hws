package com.sberuniversity.chekunov.profile.hw1;

public enum Gender {
    FEMALE("Женщина"),
    MALE("Мужчина");

    public static boolean isInstance(String gender) {
        return gender.equals(MALE.gender) || gender.equals(FEMALE.gender);
    }

    private final String gender;

    Gender(String gender) {
        this.gender = gender;
    }
}
