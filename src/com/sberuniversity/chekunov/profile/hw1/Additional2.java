package com.sberuniversity.chekunov.profile.hw1;

import java.util.Scanner;

public class Additional2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов ");
        int[] array = new int[sc.nextInt()];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("[%d] = ", i);
            array[i] = sc.nextInt();
        }
        System.out.print("Введите искомый элемент ");
        System.out.println(indexOf(array, sc.nextInt()));
    }

    public static int indexOf(int[] array, int n) {
        int left = 0;
        int right = array.length - 1;
        int middle;
        while (left <= right) {
            middle = (left + right) / 2;
            if (array[middle] == n) {
                return middle;
            } else if (array[middle] < n) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }
}
