package com.sberuniversity.chekunov.profile.hw1;

public class MyCheckedException extends Exception {
    public MyCheckedException(String message) {
        super(message);
    }
}
