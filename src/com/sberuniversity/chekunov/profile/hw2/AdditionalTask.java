package com.sberuniversity.chekunov.profile.hw2;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class AdditionalTask {

    public static String[] mostCommon(String[] words, int k) {
        Map<String, Integer> map = Task2.itemsCounter(words);
        TreeMap<String, Integer> tree = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int compare = map.get(o1).compareTo(map.get(o2));
                return compare == 0 ? o1.compareTo(o2) : compare;
            }
        }.reversed());
        tree.putAll(map);
        String[] out = new String[k];
        Iterator<String> it = tree.keySet().iterator();
        for (int i = 0; i < k && it.hasNext(); i++) {
            out[i] = it.next();
        }
        return out;
    }
}
