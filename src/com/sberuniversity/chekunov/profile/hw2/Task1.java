package com.sberuniversity.chekunov.profile.hw2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {
    public static <T> Set<T> toSet(ArrayList<T> list) {
        return new HashSet<>(list);
    }
}
