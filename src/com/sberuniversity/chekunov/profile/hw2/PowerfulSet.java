package com.sberuniversity.chekunov.profile.hw2;

import java.util.Iterator;
import java.util.Set;

public class PowerfulSet {
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        //set1.retainAll(set2);
        //set1.removeIf(t -> !set2.contains(t));
        Iterator<T> it = set1.iterator();
        while (it.hasNext()) {
            if (!set2.contains(it.next())) {
                it.remove();
            }
        }
        return set1;
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        //set1.addAll(set2);
        for (T t: set2) {
            if (!set1.contains(t)) {
                set1.add(t);
            }
        }
        return set1;
    }

    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        //set1.removeAll(set2);
        //set1.removeIf(set2::contains);
        Iterator<T> it = set1.iterator();
        while (it.hasNext()) {
            if (set2.contains(it.next())) {
                it.remove();
            }
        }
        return set1;
    }
}
