package com.sberuniversity.chekunov.profile.hw2;

import com.sberuniversity.chekunov.profile.hw2.task4.Document;
import com.sberuniversity.chekunov.profile.hw2.task4.Organizer;

import java.util.*;

public class Test {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        testAdditionalTask();
    }

    static void testTask1() {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 2, 2, 1, 5, 1, null));
        System.out.println(Task1.toSet(list));
    }

    static void testTask2() {
        /* несколько примеров:
        System.out.println(Task2.isAnagram("a", "a")); // true
        System.out.println(Task2.isAnagram("baaa", "aba")); // false
        System.out.println(Task2.isAnagram("ab", "ba")); // true
        System.out.println(Task2.isAnagram("материк", "метрика")); // true
        */
        System.out.println(Task2.isAnagram(sc.nextLine(), sc.nextLine()));
    }

    static void testTask3() {
        System.out.println(PowerfulSet.intersection(new HashSet<>(Set.of(1, 2, 3)), new HashSet<>(Set.of(0, 1, 2, 4))));
        System.out.println(PowerfulSet.union(new HashSet<>(Set.of(1, 2, 3)), new HashSet<>(Set.of(0, 1, 2, 4))));
        System.out.println(PowerfulSet.relativeComplement(new HashSet<>(Set.of(1, 2, 3)), new HashSet<>(Set.of(0, 1, 2, 4))));
    }

    static void testTask4() {
        List<Document> list = new ArrayList<>(
                List.of(
                        new Document(0, "Journal", 18),
                        new Document(5, "Logs", 438),
                        new Document(4, "Trades", 124))
        );
        System.out.println(new Organizer().organizeDocuments(list));
    }

    static void testAdditionalTask() {
        System.out.println(Arrays.toString(AdditionalTask.mostCommon(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"}, 4)));
    }
}
