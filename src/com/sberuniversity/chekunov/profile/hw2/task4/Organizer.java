package com.sberuniversity.chekunov.profile.hw2.task4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Organizer {
    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document doc : documents) {
            map.put(doc.id, doc);
        }
        return map;
    }
}
