package com.sberuniversity.chekunov.profile.hw2;

import java.util.*;

public class Task2 {
    public static boolean isAnagram(String s, String t) {
        return itemsCounter(charToCharacter(s.toCharArray())).equals(
                        itemsCounter(charToCharacter(t.toCharArray())));
    }

    public static Character[] charToCharacter(char[] array) {
        Character[] outArray = new Character[array.length];
        for (int i = 0; i < array.length; i++) {
            outArray[i] = array[i];
        }
        return outArray;
    }
    public static <T> Map<T, Integer> itemsCounter(T[] array) {
        Map<T, Integer> map = new HashMap<>();
        Arrays.sort(array);
        int i = 1, count = 1;
        for (; i < array.length; i++, count++) {
            if (array[i - 1] != array[i]) {
                map.put(array[i - 1], count);
                count = 1;
            }
            if (i + 1 == array.length) {
                map.put(array[i], count);
            }
        }
        return map;
    }
}
