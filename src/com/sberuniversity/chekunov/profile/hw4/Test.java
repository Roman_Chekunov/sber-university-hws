package com.sberuniversity.chekunov.profile.hw4;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Test {
    public static void main(String[] args) {
        System.out.println(additionalTask("abc", "abb"));
        System.out.println(additionalTask("abcd", "abb"));
        System.out.println(additionalTask("abc", "abc"));
        System.out.println(additionalTask("abc", "ab"));
    }

    static void task1() {
        System.out.println(IntStream.range(0, 101).filter(s -> s % 2 == 0).sum());
    }

    static void task2(List<Integer> list) {
        System.out.println(list.stream().reduce((x,y) -> x * y).get());
    }

    static void task3(List<String> list) {
        System.out.println(list.stream().filter(s -> !s.isEmpty()).count());
    }

    static List<Double> task4(List<Double> list) {
        return list.stream().sorted(Comparator.reverseOrder()).toList();
    }

    static void task5(List<String> list) {
        System.out.println(list.stream().map(String::toUpperCase).reduce((x, y) -> x + ", " + y).get());
    }

    static Set<Integer> task6(Set<Set<Integer>> sets) {
        return sets.stream().collect(
                HashSet::new,
                AbstractCollection::addAll,
                AbstractCollection::addAll
        );
    }

    static boolean additionalTask(String str1, String str2) {
        var chars1 = stringToChars(str1);
        chars1.removeAll(stringToChars(str2));
        return chars1.size() <= 1;
    }

    static List<Character> stringToChars(String str) {
        return str.chars()
                .mapToObj(obj -> (char) obj)
                .collect(Collectors.toList());
    }
}
