package com.sberuniversity.chekunov.hw2.p1;

import java.util.Scanner;

import static java.lang.Math.*;

public class AdditionalTask2 {
    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        for (int i : squaredArray(createArray(in.nextInt()))) {
            System.out.print(i + " ");
        }
    }

    /**
     * Ждёт от пользователя введения массива заданной длины и возвращает его
     * @param length длина создаваемого массива
     * @return массив длины <b>length</b>
     */
    private static int[] createArray(int length) {
        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            array[i] = in.nextInt();
        }

        return array;
    }

    /**
     * Возвращает отсортированный массив квадратов исходных чисел
     * @param array исходный массив
     * @return отсортированный массив типа <b>int[]</b>
     */
    private static int[] squaredArray(int[] array) {
        int length = array.length;
        int[] outArray = new int[length];
        for (int i = 0, j = length - 1, k = length - 1; i <= j; k--) {
            if (abs(array[i]) >= abs(array[j])) {
                outArray[k] = array[i] * array[i];
                i++;
            } else {
                outArray[k] = array[j] * array[j];
                j--;
            }
        }
        return outArray;
    }
}
