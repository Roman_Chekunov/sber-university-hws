package com.sberuniversity.chekunov.hw2.p1;

import java.util.Random;
import java.util.Scanner;

public class Solution_10 {
    public static void main(String[] args) {
        guessTheNumber();
    }

    /**
     * Игра в угадайку
     */
    private static void guessTheNumber() {
        // Генерируем число в промежутке [0;1000]
        // и заводим переменную для ответов пользователя.
        int number = new Random().nextInt(0, 1001), guess;

        Scanner in = new Scanner(System.in);

        // Приглашаем пользователя ввести число
        // или завершить игру.
        System.out.println(
                "Введите число от 0 до 1000.\n" +
                "Чтобы завершить игру, введите любое отрицательное число."
        );

        // Требуем у пользователя ввести число,
        // пока оно не совпадёт со сгенерированным или,
        // пока пользователь не введёт отрицательное число.
        do {
            guess = in.nextInt();
            if (guess < 0) {
                System.out.println("Игра завершена!");
                break;
            } else if (guess < number) {
                System.out.println("Это число меньше загаданного");
            } else if (guess > number) {
                System.out.println("Это число больше загаданного");
            } else {
                System.out.println("Победа!");
            }
        } while (guess != number);
    }
}