package com.sberuniversity.chekunov.hw2.p1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class AdditionalTask1 {

    private static Random random;

    /**
     * Получение переменной с инициализацией при первом вызове
     * @return проинициализированный объект {@link Random}
     */
    private static Random getRandom() {
        if (random == null) {
            random = new Random();
        }
        return random;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int passwordLength;

        // Приглашаем пользователя ввести желаемую длину пароля
        do {
            System.out.print("Введите желаемую длину пароля: ");
            passwordLength = in.nextInt();
            if (passwordLength < 8) {
                System.out.println("Пароль из " + passwordLength + " символов небезопасен");
            }
        } while (passwordLength < 8);

        System.out.println("\nСгенерирован надёжный пароль:\n" + generatePassword(passwordLength));
    }

    /**
     * Генерирует строку, состоящую из <i>passwordLength</i> символов, содержащую
     * хотя бы по одному:
     * <br>строчному латинскому символу (<b>[a-z]</b>);
     * <br>заглавному латинскому символу (<b>[A-Z]</b>);
     * <br>числовому символу (<b>[0-9]</b>);
     * <br>специальному символу (<b>[-_*]</b>);
     * @param passwordLength желаемая длина пароля
     * @return рандомно сгенерированная строка {@link String}
     */
    public static String generatePassword(int passwordLength) {
        char[] password;
        int index, randomCase;

        password = new char[passwordLength];
        Arrays.fill(password, ' ');

        // генерируем по 1 случайному символу каждого требуемого типа и
        // вставляем в случайное место в пароле
        for (int i = 0; i < 4; i++) {
            index = getRandom().nextInt(0, passwordLength);

            while (password[index] != ' ') {
                index = (index == passwordLength - 1) ? 0 : ++index;
            }

            password[index] = randomChar(i);
        }

        // заполняем оставшиеся символы
        for (int i = 4; i < passwordLength; i++) {
            index = getRandom().nextInt(0, passwordLength);
            randomCase = getRandom().nextInt(0, 4);

            while (password[index] != ' ') {
                index = (index == passwordLength - 1) ? 0 : ++index;
            }

            password[index] = randomChar(randomCase);
        }
        return String.valueOf(password);
    }

    private static char generateChar(char from, char to) {
        return (char) getRandom().nextInt(from, to);
    }

    private static char generateChar(char... symbols) {
        return symbols[getRandom().nextInt(symbols.length)];
    }

    private static char randomChar(int randomCase) {
        return switch (randomCase) {
            case 0 -> generateChar('A', 'Z');
            case 1 -> generateChar('0', '9');
            case 2 -> generateChar('-', '_', '*');
            default -> generateChar('a', 'z');
        };
    }
}
